package com.slalom.rompus.sc.arbitrator.server;

import com.slalom.rompus.sc.arbitrator.core.ServerSettings;
import com.slalom.rompus.sc.arbitrator.core.objects.Bot;
import com.slalom.rompus.sc.arbitrator.core.utility.FileUtils;
import com.slalom.rompus.sc.arbitrator.core.utility.WindowsCommandTools;

public class ServerCommands {
    public static void Server_InitialSetup() {
        if (System.getProperty("os.name").contains("Windows")) {
            ServerSettings serverSettings = ServerSettings.Instance();

            WindowsCommandTools.RunWindowsCommand("netsh firewall add allowedprogram program = " + serverSettings.ServerDir + "server.jar name = AIIDEServer mode = ENABLE scope = ALL", true, false);
            WindowsCommandTools.RunWindowsCommand("netsh firewall add portopening TCP 12345 \"Open Port 12345TCP\"", true, false);
            WindowsCommandTools.RunWindowsCommand("netsh firewall add portopening UDP 12345 \"Open Port 12345UDP\"", true, false);
            WindowsCommandTools.RunWindowsCommand("netsh firewall add portopening TCP 1337 \"Open Port 1337TCP\"", true, false);
            WindowsCommandTools.RunWindowsCommand("netsh firewall add portopening UDP 1337 \"Open Port 1337UDP\"", true, false);
            WindowsCommandTools.RunWindowsCommand("netsh firewall add portopening TCP 11337 \"Open Port 11337TCP\"", true, false);
            WindowsCommandTools.RunWindowsCommand("netsh firewall add portopening UDP 11337 \"Open Port 11337UDP\"", true, false);
        }
    }

    public static void Server_MoveWriteToRead() {
        ServerSettings serverSettings = ServerSettings.Instance();

        for (Bot bot : serverSettings.BotVector) {
            FileUtils.CopyDirectory(serverSettings.getBotWriteDirectory(bot), serverSettings.getBotReadDirectory(bot));

            //String copy = "xcopy " + ServerSettings.Instance().ServerBotDir + bot.getName() + "/write/*.* " + ServerSettings.Instance().ServerBotDir + bot.getName() + "/read/ /E /V /Y";
            //WindowsCommandTools.RunWindowsCommand(copy, true, false);

            //WindowsCommandTools.CopyDirectory(ServerSettings.Instance().ServerBotDir + bot.getName() + "/write/", ServerSettings.Instance().ServerBotDir + bot.getName() + "/read/");
        }
    }
}