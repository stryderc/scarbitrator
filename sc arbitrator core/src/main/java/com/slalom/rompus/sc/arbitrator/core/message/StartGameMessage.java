package com.slalom.rompus.sc.arbitrator.core.message;

import com.slalom.rompus.sc.arbitrator.core.interfaces.Message;

public class StartGameMessage implements Message {
    private static final long serialVersionUID = -347052767995307052L;

    public StartGameMessage() {

    }

    public String toString() {
        return "Start The Game Already!";
    }
}