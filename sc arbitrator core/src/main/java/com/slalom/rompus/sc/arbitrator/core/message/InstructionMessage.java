package com.slalom.rompus.sc.arbitrator.core.message;

import com.slalom.rompus.sc.arbitrator.core.game.Game;
import com.slalom.rompus.sc.arbitrator.core.interfaces.Message;
import com.slalom.rompus.sc.arbitrator.core.objects.BWAPISettings;
import com.slalom.rompus.sc.arbitrator.core.objects.Bot;

public class InstructionMessage implements Message {
    private static final long serialVersionUID = 9062722137863042773L;

    public Bot hostBot;
    public Bot awayBot;

    public boolean isHost;

    public BWAPISettings bwapi;

    public int game_id;
    public int round_id;

    public InstructionMessage() {

    }

    public InstructionMessage(BWAPISettings defaultBWAPI, boolean host, Game game) {
        hostBot = game.getHomebot();
        awayBot = game.getAwaybot();
        game_id = game.getGameID();
        round_id = game.getRound();
        isHost = host;
        try {
            bwapi = defaultBWAPI.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        bwapi.enemy_count = "1";
        bwapi.wait_for_max_players = 2;
        bwapi.map = host ? game.getMap().getMapLocation() : "";
        bwapi.auto_menu = "LAN";
    }

    public String toString() {
        return hostBot.getName() + " " + awayBot.getName() + " " + isHost + " (" + game_id + "/" + round_id + ")";
    }
}
