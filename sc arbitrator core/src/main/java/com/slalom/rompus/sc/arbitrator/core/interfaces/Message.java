package com.slalom.rompus.sc.arbitrator.core.interfaces;

import java.io.Serializable;

public interface Message extends Serializable {
    public String toString();
}