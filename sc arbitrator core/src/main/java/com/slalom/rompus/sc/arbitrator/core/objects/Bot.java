package com.slalom.rompus.sc.arbitrator.core.objects;

import java.io.Serializable;

public class Bot implements Serializable {

    private static final long serialVersionUID = 4800327650045375901L;

    private String name;
    private String race;
    private String type;
    private String bwapiVersion;
    private String serverBotDir;
    private String serverRequiredDir;

    protected Bot(String name, String race, String type, String bwapiVersion) {
        this.name = name;
        this.race = race;
        this.type = type;
        this.bwapiVersion = bwapiVersion;
    }

    private Bot(Builder builder) {
        name = builder.name;
        race = builder.race;
        type = builder.type;
        bwapiVersion = builder.bwapiVersion;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getName() {
        return name;
    }

    public String getRace() {
        return race;
    }

    public String getType() {
        return type;
    }


    public String getBWAPIVersion() {
        return bwapiVersion;
    }

    public boolean isProxyBot() {
        return type.equalsIgnoreCase("proxy");
    }

    public void setServerBotDir(String serverBotDir) {
        this.serverBotDir = serverBotDir;
    }

    public void setServerRequiredDir(String serverRequiredDir) {
        this.serverRequiredDir = serverRequiredDir;
    }

    public String getBotBWAPIReq() {
        return serverRequiredDir + "Required_" + getBWAPIVersion() + ".zip";
    }

    public String getBotProxyScript() {
        return getBotAiDirectory() + "run_proxy.bat";
    }

    public String getBotDll() {
        return getBotAiDirectory() + getName() + ".dll";
    }

    public String getBotAiDirectory() {
        return getBotDirectory() + "AI/";
    }

    public String getBotDirectory() {
        return serverBotDir + getName() + "/";
    }

    public String getBotWriteDirectory() {
        return serverBotDir + getName() + "/write/";
    }

    public String getBotReadDirectory() {
        return serverBotDir + getName() + "/read/";
    }

    public static final class Builder {
        private String name;
        private String race;
        private String type;
        private String bwapiVersion;

        private Builder() {
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withRace(String race) {
            this.race = race;
            return this;
        }

        public Builder withType(String type) {
            this.type = type;
            return this;
        }

        public Builder withBwapiVersion(String bwapiVersion) {
            this.bwapiVersion = bwapiVersion;
            return this;
        }

        public Bot build() {
            return new Bot(this);
        }
    }
}
